from django.http import HttpResponse
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.shortcuts import *

from undangan.models import Undangan

def CariUndangan(request):
    if request.GET.get('kode'):
        object = get_object_or_404(Undangan, kode=request.GET['kode'])
        # Undangan.objects.get(kode=request.GET['kode'])
    else:
        message = 'You submitted nothing!'
    context = {'object' : object}
    return render(request, 'undangan_detail.html', context)

class UndanganView(DetailView):
    model = Undangan

def index(request):
    return render (request, 'welcome.html')
