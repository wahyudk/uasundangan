from django.urls import path

from . import views

urlpatterns = [
    path('', views.UndanganList.as_view(), name='undangan_list'),
    path('new', views.UndanganCreate.as_view(extra_context={'title': 'Tambah Undangan'}), name='undangan_new'),
    path('edit/<int:pk>', views.UndanganUpdate.as_view(extra_context={'title': 'Edit Undangan'}), name='undangan_edit'),
    path('delete/<int:pk>', views.UndanganDelete.as_view(), name='undangan_delete'),
]
