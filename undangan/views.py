from django.http import HttpResponse
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from .models import Undangan

# def CariUndangan(request):
#     if request.GET.get('kode'):
#         object = Undangan.objects.get(kode=request.GET['kode'])
#     else:
#         message = 'You submitted nothing!'
#     context = {'object' : object}
#     return render(request, 'undangan_detail.html', context)

class UndanganList(ListView):
    model = Undangan

class UndanganCreate(CreateView):
    model = Undangan
    fields = ['judul', 'kode', 'venue', 'alamat', 'tanggal', 'story']
    success_url = reverse_lazy('undangan_list')

class UndanganUpdate(UpdateView):
    model = Undangan
    fields = ['judul', 'kode', 'venue', 'alamat', 'tanggal', 'story']
    success_url = reverse_lazy('undangan_list')

class UndanganDelete(DeleteView):
    model = Undangan
    success_url = reverse_lazy('undangan_list')
