from django.db import models

class Undangan(models.Model):
    judul = models.CharField(max_length=75)
    kode = models.CharField(max_length=20)
    venue = models.CharField(max_length=50)
    alamat = models.TextField()
    tanggal = models.DateTimeField()
    story = models.TextField()

    def __str__(self):
        return self.judul
